# ep3

Exercício Programa 3 OO FGA 2018/2
## Objetivo do Projeto
* Controlo é uma plataforma de controle de equipes(exemplos:equipe de competição,empresas juniores,equipes esportivas) qeu visa facilitar a acelerar o processo de administração das finanças da equipe.

## Como Funciona
* O tesoureiro da equipe deve se cadastrar e adicionar os integrantes de sua equipe. Ele será responsável em manipular a tabela de controle e do caixa da equipe.
* Assim que ele adiciona seus membros, ele enviará um link de acesso para os respectivos membros.
* Assim que o convidado recebe seu convite, se ele não tiver cadastro, ele se cadastra e aceita o convida.Se ele tiver cadastro, ele apenas aceita o convite e se torna membro da equipe.
* Os membros tem acesso à página de controle, mas apenas para visualização.
* Somente o tesoureiro tem acesso as funcionalidades da tabela.

## Benefícios do Projeto
* Com essa plataforma, a forma de se tratar com o financeiro das equipes se torna transparente, pois todos possuem acesso ao controle, tanto como os que já contribuíram no mês, como ao caixa da equipe. Além de facilitar o acesso para todos os integrantes, pois agora, quando um integrante desejar visualizar o controle, ele não precisa mais entrar em contato com o tesoureiro, e sim apenas verificar no site.

## O que foi utilizado
* Utilizamos a gem devise para fazer o processo de autenticação dos usuários.
* Não utilizamos gems para a criação de layouts.