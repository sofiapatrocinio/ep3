class AddViewerToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :viewer, :boolean, default: false
  end
end
