Rails.application.routes.draw do
  root to: 'pages#home'
  devise_for :users
  get 'group' => 'pages#group', as: 'group'
  get 'admin' => 'pages#admin', as: 'admin'
  get 'table' => 'pages#table', as: 'table'
  get 'invite' => 'pages#invite', as: 'invite'
  get 'team' => 'pages#team', as: 'team'
  get 'table1' => 'pages#table1', as: 'table1'
  resources :members
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
