class MembersController < ApplicationController

    
    def show
        @members = Member.all
        
    end
    
    def update
        @member = Member.last
        Member.all.each do |aux|
        aux.update(member_params)
        end
     end

    def create
        @member = Member.new(member_params)

        if @member.save
            redirect_to admin_path
        else
            render "admin"
        end
     end

    
     
    private
    def member_params
        params.required(:member).permit(:firstname, :lastname, :email, :cash)
    end
end
