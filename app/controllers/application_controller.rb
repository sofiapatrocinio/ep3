class ApplicationController < ActionController::Base
    

    protect_from_forgery with: :exception
  
    before_action :set_redirect_path, unless: :user_signed_in?
  
    def set_redirect_path
      @redirect_path = request.path
    end
  
    def after_sign_in_path_for(resource)
      if params[:redirect_to].present?
        store_location_for(resource, params[:redirect_to])
      elsif request.referer == new_session_url
        super
      else
        stored_location_for(resource) || request.referer || root_path
      end
    end

    def after_sign_up_path_for(resource)
        if params[:redirect_to].present?
          store_location_for(resource, params[:redirect_to])
        elsif request.referer == new_session_url
          super
        else
          stored_location_for(resource) || request.referer || root_path
        end
      end

  end
